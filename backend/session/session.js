import { v4 as uuidv4 } from 'uuid';
import session from 'express-session';


export const configureSession = () => {
	return session({
	  secret: 'очень секретный ключ',
	  resave: false,
	  saveUninitialized: true,
	  genid: (req) => {
		return uuidv4();
	  },
	});
  };