import express from 'express';
import { configureSession } from './session/session.js'
import fs from 'fs';
import cors from 'cors';

const app = express();
const HOST = process.env.HOST || '127.0.0.1';
const PORT = process.env.PORT || 4444;

app.use(express.json());
app.use(cors());
app.use('/uploads', express.static('uploads')); 

let userId;
let saveData = {};

app.use(configureSession());  

app.get('/quiz', (req, res) => {
  try {
    const surveyData = fs.readFileSync('./quiz.json', 'utf-8');
    const parsedData = JSON.parse(surveyData);
    userId = req.sessionID

    if (!saveData[userId]) {
      saveData[userId] = {
        fullInfo: parsedData.fullInfo,
        results: parsedData.results[0],
        questions: parsedData.questions,
        step: 0,
        count: 0,
      };
    }


    const { questions, step, count } = saveData[userId];
    res.json({ questions, step, count });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});

app.post('/quiz', (req, res) => {
  try {
    const data = req.body;

    saveData[userId] = {
      ...saveData[userId],
      questions: data.questions,
      step: data.step + 1,
      count: data.count,
    };

    const nextResult = saveData[userId].step + 1 === saveData[userId].questions.length;

    res.json({ step: saveData[userId].step, count: saveData[userId].count, nextResult });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


app.get('/result', (req, res) => {
  try {
    if (!saveData[userId]) {
      saveData[userId] = {
        step: 0,
        count: 0,
      };
    }

    const { count, results, fullInfo, step } = saveData[userId];

    res.json({ count, results, fullInfo, step });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
});


app.get('/full-info', (req, res) => {
  try {
    if (!saveData[userId]) {
      saveData[userId] = {
        step: 0,
        count: 0,
      };
    }
    
    const { fullInfo } = saveData[userId];

    res.json({ fullInfo });
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})


app.get('/result/directions/:num', (req, res) => {
  try {
    if (!saveData[userId]) {
      saveData[userId] = {
        step: 0,
        count: 0,
      };
    }
    const { fullInfo } = saveData[userId];

    res.json({ fullInfo })
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: 'Internal Server Error' });
  }
})


app.listen(PORT, HOST, (err) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log(`Server is running on http://${HOST}:${PORT}`);
});
